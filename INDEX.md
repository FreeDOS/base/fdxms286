# fdxms286, version 0.03.Temperaments (rev A)

Replacement XMS driver for '286 systems or better.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

# FDXMS286.LSM

<table>
<tr><td>title</td><td>fdxms286</td></tr>
<tr><td>version</td><td>0.03.Temperaments (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2005-05-28</td></tr>
<tr><td>description</td><td>Replacement XMS driver for '286 systems or better.</td></tr>
<tr><td>summary</td><td>Replacement XMS driver for '286 systems or better.</td></tr>
<tr><td>keywords</td><td>freedos, xms, himem</td></tr>
<tr><td>author</td><td>Till Gerken <till@tantalo.net>, Martin Strimberg <ams@ludd.ltu.se></td></tr>
<tr><td>maintained&nbsp;by</td><td>Martin Strimberg <ams@ludd.ltu.se></td></tr>
<tr><td>primary&nbsp;site</td><td>[http://www.ludd.ltu.se/~ams/freedos/](http://www.ludd.ltu.se/~ams/freedos/)</td></tr>
<tr><td>original&nbsp;site</td><td>[http://www.ludd.ltu.se/~ams/freedos/](http://www.ludd.ltu.se/~ams/freedos/)</td></tr>
<tr><td>platforms</td><td>dos (ASM)</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
</table>
